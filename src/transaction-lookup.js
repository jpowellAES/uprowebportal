import { inject } from 'aurelia-dependency-injection';
import ApiService from 'components/ApiService';
import {Notification} from 'aurelia-notification';
import "humane-js/themes/libnotify.css!";

@inject(ApiService,Notification)
export class TransactionLookup {

  constructor(ApiService, Notification) {
    this.api = ApiService;
    this.notify = Notification;
    this.txn = {
      name: '',
      when: ''
    }
    this.loading = false;
    this.table = {
        headers: [],
        rows: [],
        ignoreCols: []
    };
  }

  search() {
    this.loading = true;
    let _this = this;
    this.api.post('readset',this.txn.name+':'+this.txn.when)
        .then((res)=>{
            _this.parseResponse(res);
            _this.loading = false;
        },(err)=>{
            _this.notify.error(err);
            _this.loading = false;
        });
    }

    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    parseResponseHeaders(obj){
        let ignore = this.table.ignoreCols.filter(this.onlyUnique);
        let indices = [], i = 0;
        for(let key in obj){
            if(ignore.indexOf(key) > -1){
                indices.push(i);
            }else{
                this.table.headers.push(key);
                ++i;
            }
            for(let j=0; j<indices.length; j++){
                for(let k=0; k<this.table.rows.length; k++){
                    this.table.rows[k].splice(indices[j], 1);
                }
            }
        }
    }
    parseResponseRow(obj){
        let row = [];
        for(let key in obj){
            if(typeof obj[key] == 'object' || Array.isArray(obj[key])){
                this.table.ignoreCols.push(key);
            }
            row.push(obj[key]);
        }
        this.table.rows.push(row);
    }
    parseResponse(res){
        this.txn.data = res;
        this.table.headers = [];
        this.table.rows = [];
        this.table.ignoreCols = [];
        for(let key in res){
            if(Array.isArray(res[key])){
                for(let i=0; i<res[key].length; i++){
                    this.parseResponseRow(res[key][i])
                }
                this.parseResponseHeaders(res[key][0]);
            }else{
                this.parseResponseRow(res[key]);
                this.parseResponseHeaders(res[key]);
            }
            return;
        }
    }
}

