﻿import { inject, Factory } from 'aurelia-framework';
import { HttpClient } from 'aurelia-http-client';

@inject(Factory.of(HttpClient))
class SoapClient {

    static cacheWsdl = [];
    
    constructor(options: object, url: string): SoapClient {
        let opt = options || {};
        this.url = url || null;
        this.method = null;
        this.parameters = null;
        this.wsdl = null;
        this.callback = null;
        this.certFix = false;
        this.options = {
            wsdlUrl: opt.wsdlUrl || null
        };
        if (this.url == null && this.options.wsdlUrl == null) {
            throw new Error("You must specify a URL or a WSDL with a URL defined");
        }
        this.http = new HttpClient();
    }

    invoke(method: string, parameters: SoapParameters): Promise {
        let _this = this;
        this.method = method;
        this.parameters = parameters;
        return new Promise(function(resolve, reject){
            _this.resolve = resolve;
            _this.reject = reject;
            _this.loadWsdl();
        });
    }

    loadWsdl() {
        if (this.wsdl)
            return this.sendSoapRequest();
        // load from cache?
        let wsdl = SoapClient.cacheWsdl[this.url];
        if (wsdl + "" != "" && wsdl + "" != "undefined") {
            this.wsdl = wsdl;
            return this.sendSoapRequest();
        }
        // get wsdl
        let wsdlUrl = this.options.wsdlUrl || this.url + "?wsdl";
        this.http.get(wsdlUrl)
            .then((response) => this.onLoadWsdl(response), (error) => this.reject(error));
    }

    onLoadWsdl (req) {
        let wsdl = SoapClient.parseXML(req.response);
        SoapClient.cacheWsdl[this.url] = wsdl; // save a copy in cache
        this.wsdl = wsdl;
        if(this.url == null){
            this.url = SoapClient.getURLFromWSDL(this.wsdl);
        }
        this.sendSoapRequest();
    }

    fixCertProblem() {
        this.certFix = true;
        this.http.get(this.url)
            .then((response) => this.sendSoapRequest(), (error) => this.reject(error));
    }

    sendSoapRequest () {
        if (!this.certFix) {
            return this.fixCertProblem();
        }
        // get namespace
        let ns = (this.wsdl.documentElement.attributes["targetNamespace"] + "" == "undefined") ? this.wsdl.documentElement.attributes.getNamedItem("targetNamespace").nodeValue : this.wsdl.documentElement.attributes["targetNamespace"].value;
        // build SOAP request
        let soapData =
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                    "<soap:Envelope " +
                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                    "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                    "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                    "<soap:Body>" +
                    "<" + this.method + " xmlns=\"" + ns + "\">" +
                    this.parameters.toXml() +
                    "</" + this.method + "></soap:Body></soap:Envelope>";
        // send request
        let soapaction = SoapClient.getSoapAction(ns, this.method, this.wsdl);
        this.http.createRequest(this.url)
            .withHeader("SOAPAction", soapaction)
            .withHeader("Content-Type", "text/xml; charset=utf-8")
            .withContent(soapData)
            .asPost()
            .send()
            .then((response) => this.onSendSoapRequest(response), (error) => this.reject(error));
    }

    onSendSoapRequest (req) {
        let o = null;
        let xml = SoapClient.parseXML(req.response) || null;
        if (xml) {
            let methodResponseNode = SoapClient.getMethodResponseNode(this.method, this.wsdl);
            let nd = SoapClient.getElementsByTagName(xml, methodResponseNode);
            if (nd.length == 0) {
                if (xml.getElementsByTagName("faultcode").length > 0) {
                    this.reject(xml.getElementsByTagName("faultstring")[0].childNodes[0].nodeValue);
                }
            }
            else
                o = SoapClient.soapResult2Object(nd[0], this.wsdl);
        }
        this.resolve(o);
    }

   static getURLFromWSDL(wsdl: XMLDocument): string {
        let address = wsdl.getElementsByTagName('soap:address');
        if (address.length && address[0].attributes.location)
            return address[0].attributes.location.value;
    }

    static parseXML(txt: string): XMLDocument {
        let xmlDoc;
        if (window.DOMParser){
            let parser = new DOMParser();
            xmlDoc = parser.parseFromString(txt, "text/xml");
        }else{
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(txt);
        }
        return xmlDoc;
    }

    static getSoapAction(ns: string, method: string, wsdl: XMLDocument): string {
        let nodes = wsdl.documentElement.childNodes;
        for (let i = 0; i < nodes.length; i++) {
            if (nodes[i].nodeName == 'service' && nodes[i].attributes && nodes[i].attributes.name) {
                return nodes[i].attributes.name.value;
            }
        }
        return ((ns.lastIndexOf("/") != ns.length - 1) ? ns + "/" : ns) + method;
    }

    static getMethodResponseNode (method: string, wsdl: XMLDocument) {
        let operations = wsdl.getElementsByTagName("operation");
        for (let i = 0; i < operations.length; i++) {
            if (operations[i].attributes.name && operations[i].attributes.name.nodeValue == method) {
                let out = operations[i].getElementsByTagName("output");
                if (out.length && out[0].attributes.message) {
                    let val = out[0].attributes.message.value;
                    return val.indexOf(':') > 0 ? val.split(':')[1] : val;
                }
            }
        }
        return method + "Result";
    }

    static soapResult2Object (node, wsdl: XMLDocument) {
        let wsdlTypes = SoapClient.getTypesFromWsdl(wsdl);
        return SoapClient.node2Object(node, wsdlTypes);
    }

    static node2Object (node, wsdlTypes) {
        // null node
        if (node == null)
            return null;
        // text node
        if (node.nodeType == 3 || node.nodeType == 4)
            return SoapClient.extractValue(node, wsdlTypes);
        // leaf node
        if (node.childNodes.length == 1 && (node.childNodes[0].nodeType == 3 || node.childNodes[0].nodeType == 4))
            return SoapClient.node2Object(node.childNodes[0], wsdlTypes);
        let isarray = SoapClient.getTypeFromWsdl(node.nodeName, wsdlTypes).toLowerCase().indexOf("arrayof") != -1;
        // object node
        if (!isarray) {
            let obj = null;
            if (node.hasChildNodes())
                obj = new Object();
            for (let i = 0; i < node.childNodes.length; i++) {
                let p = SoapClient.node2Object(node.childNodes[i], wsdlTypes);
                obj[node.childNodes[i].nodeName] = p;
            }
            return obj;
        }
            // list node
        else {
            // create node ref
            let l = new Array();
            for (let i = 0; i < node.childNodes.length; i++)
                l[l.length] = SoapClient.node2Object(node.childNodes[i], wsdlTypes);
            return l;
        }
    }

    static extractValue (node, wsdlTypes) {
        let value = node.nodeValue;
        switch (SoapClient.getTypeFromWsdl(node.parentNode.nodeName, wsdlTypes).toLowerCase()) {
            default:
            case "s:string":
                return (value != null) ? value + "" : "";
            case "s:boolean":
                return value + "" == "true";
            case "s:int":
            case "s:long":
                return (value != null) ? parseInt(value + "", 10) : 0;
            case "s:double":
                return (value != null) ? parseFloat(value + "") : 0;
            case "s:datetime":
                if (value == null)
                    return null;
                else {
                    value = value + "";
                    value = value.substring(0, (value.lastIndexOf(".") == -1 ? value.length : value.lastIndexOf(".")));
                    value = value.replace(/T/gi, " ");
                    value = value.replace(/-/gi, "/");
                    let d = new Date();
                    d.setTime(Date.parse(value));
                    return d;
                }
        }
    }

    static getTypesFromWsdl (wsdl: XMLDocument) {
        let wsdlTypes = new Array();
        // IE
        let ell = wsdl.getElementsByTagName("s:element");
        let useNamedItem = true;
        // MOZ
        if (ell.length == 0) {
            ell = wsdl.getElementsByTagName("element");
            useNamedItem = false;
        }
        for (let i = 0; i < ell.length; i++) {
            if (useNamedItem) {
                if (ell[i].attributes.getNamedItem("name") != null && ell[i].attributes.getNamedItem("type") != null)
                    wsdlTypes[ell[i].attributes.getNamedItem("name").nodeValue] = ell[i].attributes.getNamedItem("type").nodeValue;
            }
            else {
                if (ell[i].attributes["name"] != null && ell[i].attributes["type"] != null)
                    wsdlTypes[ell[i].attributes["name"].value] = ell[i].attributes["type"].value;
            }
        }
        return wsdlTypes;
    }
    
    static getTypeFromWsdl (elementname: string, wsdlTypes) {
        let type = wsdlTypes[elementname] + "";
        return (type == "undefined") ? "" : type;
    }

    static getElementsByTagName (document, tagName) {
        if (!document)
            return [];
        try {
            // trying to get node omitting any namespaces (latest versions of MSXML.XMLDocument)
            return document.selectNodes(".//*[local-name()=\"" + tagName + "\"]");
        }
        catch (ex) { }
        // old XML parser support
        return document.getElementsByTagName(tagName);
    }
}

class SoapParameters {

    constructor(fromObj: object): SoapParameters {
        this.parameters = {};
        if(fromObj){
            for(let prop in fromObj){
                this.add(prop, fromObj[prop]);
            }
        }
    }

    add(name: string, val:value): SoapParameters {
        this.parameters[name] = val;
        return this;
    }

    toXml(): string {
        let xml = '';
        for(let name in this.parameters){
            xml += '<' + name + '>' + SoapParameters.serialize(this.parameters[name]) + '</' + name + '>';
        }
        return xml;
    }

    static serialize(o: object): string {
        let s = '';
        switch (typeof (o)) {
            case 'string':
                s += o.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                break;
            case 'number':
            case 'boolean':
                s += o.toString();
                break;
            case 'object':
                if (!o)
                    return '';
                // Date
                if (o.constructor.toString().indexOf('function Date()') > -1) {
                    let year = o.getFullYear().toString();
                    let mo = (o.getMonth() + 1).toString(); let month = (mo.length == 1) ? '0' + mo : mo;
                    let d = o.getDate().toString(); let date = (d.length == 1) ? '0' + d : d;
                    let h = o.getHours().toString(); let hours = (h.length == 1) ? '0' + h : h;
                    let mi = o.getMinutes().toString(); let minutes = (mi.length == 1) ? '0' + mi : mi;
                    let s = o.getSeconds().toString(); let seconds = (s.length == 1) ? '0' + s : s;
                    let milliseconds = o.getMilliseconds().toString();
                    let tzminutes = Math.abs(o.getTimezoneOffset());
                    let tzhours = 0;
                    while (tzminutes >= 60) {
                        tzhours++;
                        tzminutes -= 60;
                    }
                    tzminutes = (tzminutes.toString().length == 1) ? '0' + tzminutes.toString() : tzminutes.toString();
                    tzhours = (tzhours.toString().length == 1) ? '0' + tzhours.toString() : tzhours.toString();
                    let timezone = ((o.getTimezoneOffset() < 0) ? '+' : '-') + tzhours + ':' + tzminutes;
                    s += year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds + '.' + milliseconds + timezone;
                }
                    // Array
                else if (o.constructor.toString().indexOf('function Array()') > -1) {
                    for (let p in o) {
                        if (!isNaN(p))   // linear array
                        {
                            (/function\s+(\w*)\s*\(/ig).exec(o[p].constructor.toString());
                            let type = RegExp.$1;
                            switch (type) {
                                case '':
                                    type = typeof (o[p]);
                                case 'String':
                                    type = 'string';
                                    break;
                                case 'Number':
                                    type = 'int';
                                    break;
                                case 'Boolean':
                                    type = 'bool';
                                    break;
                                case 'Date':
                                    type = 'DateTime';
                                    break;
                            }
                            s += '<' + type + '>' + SoapParameters.serialize(o[p]) + '</' + type + '>'
                        }
                        else    // associative array
                            s += '<' + p + '>' + SoapParameters.serialize(o[p]) + '</' + p + '>'
                    }
                }
                    // Object or custom function
                else
                    for (let p in o)
                        s += '<' + p + '>' + SoapParameters.serialize(o[p]) + '</' + p + '>';
                break;
            default:
                throw new Error(500, 'SoapParameters: type ' + typeof (o) + ' is not supported');
        }
        return s;
    }
}

export {
    SoapClient,
    SoapParameters
}