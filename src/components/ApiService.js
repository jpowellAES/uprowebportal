import { inject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-http-client';
import AuthService from 'components/AuthService';
import config from 'config';

@inject(HttpClient,AuthService)
export default class ApiService {

	constructor(HttpClient,AuthService) {
		HttpClient.configure(http => {
	      	http.withBaseUrl(config.baseUrl);
    	});

		this.http = HttpClient;
		this.auth = AuthService;
	}

	post(route, data) {
		let _this = this;
		return new Promise(function(resolve, reject){
			_this.http
			.post(config.apiUrl+route, { data: data })
			.then((response) => {
				_this.auth.renew();
				resolve(response.content);
			}, (error) => {
				_this.checkResponseError(error.content);
				reject(error.content.message||"");
			})
		});
	}

	get(route) {
		let _this = this;
		return new Promise(function(resolve, reject){
			_this.http
			.get(config.apiUrl+route)
			.then((response) => {
				_this.auth.renew();
				resolve(response.content);
			}, (error) => {
				_this.checkResponseError(error.content);
				reject(error.content.message||"");
			})
		});
	}

	checkResponseError(error){
		if(error.reason && error.reason == "expiredSession")
			this.auth.clearSession();
	}
}