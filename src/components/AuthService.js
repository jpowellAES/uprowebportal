import { Aurelia, inject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-http-client';
import config from 'config';

@inject(Aurelia, HttpClient)
export default class AuthService {

	session = null

	// As soon as the AuthService is created, we query local storage to
	// see if the login information has been stored. If so, we immediately
	// load it into the session object on the AuthService.
	constructor(Aurelia, HttpClient) {
		HttpClient.configure(http => {
	      	http.withBaseUrl(config.baseUrl);
    	});

		this.http = HttpClient;
		this.app = Aurelia;

		this.restore();
	}

	login(username, password) {
		let _this = this;
		return new Promise(function(resolve, reject){
			_this.http
			.post(config.loginUrl, { username, password })
			.then((response) => {
				if(response.content.success){
					_this.session = response.content;
					_this.save();
					_this.app.setRoot('app');
					resolve();
				}else{
					reject(response.content.message||"");
				}
			}, (error) => {
				reject();
			})
		});
	}

	getName(){
		if(this.session && this.session['name'])
			return this.session['name'];
		return 'Account';
	}

	setName(name){
		if(this.session){
			this.session['name'] = name;
			this.save();
		}
	}

	logout() {
		let _this = this;
		this.http
			.post(config.logoutUrl)
			.then((response) => {
				console.log(response.content);
				_this.clearSession();
			}, () => _this.clearSession());
		
	}

	clearSession(){
		localStorage[config.tokenName] = null;
		this.session = null;
		this.app.setRoot('login')
	}
	
	isAuthenticated() {
		if(this.session == null) return false;
		return this.checkSessionTimeout();
	}

	restore(){
		this.session = JSON.parse(localStorage[config.tokenName] || null);
		if(this.session){
			this.checkSessionTimeout();
		}
	}

	save(){
		if(this.session && !this.session['expires']){
			this.session['expires'] = new Date().getTime() + (this.session['lengthMinutes']||20)*60*1000;
		}
		localStorage[config.tokenName] = JSON.stringify(this.session);
	}

	renew(){
		this.session['expires'] = new Date().getTime() + (this.session['lengthMinutes']||20)*60*1000;
		localStorage[config.tokenName] = JSON.stringify(this.session);
	}

	checkSessionTimeout(){
		if(!this.session['expires'] || this.session['expires'] <= new Date().getTime()){
			this.session = null;
			this.save();
			return false;
		}
		return true;
	}
}