// The confic could be part of a larger config file. If this service
// was abstracted to a plugin, it could be set up in the config.
export default {
	baseUrl: 'http://localhost:56697/',
	apiUrl: 'http://localhost:56697/api/',
	loginUrl: 'account/login',
	logoutUrl: 'account/logout',
	tokenName: 'fPClwzjtz0VbYzWbBF2w'
};