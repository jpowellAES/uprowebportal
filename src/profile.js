import { inject } from 'aurelia-dependency-injection';
import ApiService from 'components/ApiService';
import AuthService from 'components/AuthService';
import {Notification} from 'aurelia-notification';
import "humane-js/themes/libnotify.css!";

@inject(ApiService,AuthService,Notification)
export class Profile {

    isDirty = false;

    constructor(ApiService, AuthService, Notification) {
        this.api = ApiService;
        this.auth = AuthService;
        this.notify = Notification;
        this.user = {};
        this.loading = false;
        this.getProfile();
    }

    getProfile(){
        this.loading = true;
        let _this = this;
        this.api.get('user/profile/me')
            .then((res) => {
                _this.user = res.EndUsers;
                _this.loading = false;
            }, (err) => {
                _this.notify.error(err);
                _this.loading = false;
            });
    }

    saveProfile(){
        this.loading = true;
        let _this = this;
        this.api.post('user/profile/me', this.user)
            .then((res) => {
                _this.saveSuccess();
                _this.loading = false;
                
            }, (err) => {
                _this.notify.error(err);
                _this.loading = false;
            });
        
    }

    saveSuccess(){
        this.isDirty = false;
        this.auth.setName(this.user.Person.FirstName +' '+this.user.Person.LastName);
        this.notify.success("Changes saved");
    }
}

export class ToJSONValueConverter {
  toView(obj) {
    if (obj) {
      return JSON.stringify(obj, null, 2)
    }
  }
}