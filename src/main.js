import AuthService from 'components/AuthService';

export function configure(aurelia) {
	aurelia.use
    .standardConfiguration()
    .plugin('aurelia-notification', config => {
      config.configure({
        translate: false,  // 'true' needs aurelia-i18n to be configured
        timeout: 8000,
        notifications: {
          'success': 'humane-libnotify-success',
          'error': 'humane-libnotify-error',
          'info': 'humane-libnotify-info'
        }
      });
    })
    .developmentLogging();

  // After starting the aurelia, we can request the AuthService directly
  // from the DI container on the aurelia object. We can then set the 
  // correct root by querying the AuthService's isAuthenticated method.
  aurelia.start().then(() => {
	  	var auth = aurelia.container.get(AuthService);
	    let root = auth.isAuthenticated() ? 'app' : 'login';
	    aurelia.setRoot(root);
  	});
}
