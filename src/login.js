import AuthService from 'components/AuthService';
import { inject } from 'aurelia-dependency-injection';

@inject(AuthService)
export class Login {

  constructor(AuthService) {
    this.isLoggingIn = false;
    // Or, if we want to add additional logic to the function, 
    // we can call it within another method on our view model.
    this.login = () => {
      if (this.username && this.password) {
        this.isLoggingIn = true;
        this.error = '';
        AuthService.login(this.username, this.password)
          .then(()=>{}, (msg) => this.error = msg||'Failed to connect')
          .then(()=> {this.isLoggingIn = false})
      } else {
        this.error = 'Please enter a username and password.';
      }
    }
  }

  activate() {
    this.username = '';
    this.password = '';
    this.error = '';
  }

}
