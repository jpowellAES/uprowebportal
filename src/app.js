import 'bootstrap';
import { inject, Factory } from 'aurelia-dependency-injection';
import AuthService from 'components/AuthService';
import ApiService from 'components/ApiService';
import { SoapClient, SoapParameters } from 'components/SoapClient';

@inject(AuthService,ApiService,Factory.of(SoapClient),Factory.of(SoapParameters))
export class App {

  constructor(AuthService, ApiService) {
  	this.auth = AuthService;
    this.api = ApiService;

  }

  configureRouter(config, router) {
    this.router = router;
    config.title = 'UnitsPro';
    config.map([
       { route: '' }
    ]);
  }

  goToProfile(){
    console.log()
    $('#tab-profile').tab('show');
  }

  activate(){
    // let client = new SoapClient({wsdlUrl:'src/wsdl/upro.wsdl'});
    // let params = new SoapParameters({
    //   TFWSUser: 'jpowell',
    //   TFWSPassword: 'Avalanche1',
    //   HttpMethod: 'GET',
    //   Route: 'readset',
    //   Data: 'GraphicFXN:'
    // });

    // client.invoke('PortalCall', params)
    //   .then((res) => console.log(res))
  }
}